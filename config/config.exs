# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# bookapo_admin configuration:

# pow configuration for admin app
config :bookapo_admin, :pow,
  repo: Bookapo.Repo,
  user: Bookapo.Accounts.Admin,
  web_module: BookapoAdminWeb

# Configures the endpoint
config :bookapo_admin, BookapoAdminWeb.Endpoint,
  url: [host: "admin.localhost"],
  secret_key_base: "gJlrMLHoFmju2vLpATxcPWp5hc0Lu6PqiWk8b2pAYO8U1oC1Bp1ubiCORiMqZ2Zh",
  render_errors: [view: BookapoAdminWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: BookapoAdmin.PubSub,
  live_view: [signing_salt: "HABjQgK2"],
  server: false


# bookapo_web configuration:

# pow configuration for web app
config :bookapo_web, :pow,
  web_module: BookapoWeb

# Configures the endpoint
config :bookapo_web, BookapoWeb.Endpoint,
  url: [host: "localhost"],
  http: [port: 4000],
  secret_key_base: "rguIxl1Lv48Xqp9ThEF8YWNBaytXyEyvZrMZ/UMI1eMDgDwRDC492MFJXXWDcgIR",
  render_errors: [view: BookapoWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Bookapo.PubSub,
  live_view: [signing_salt: "sdDJakp9"]

## Repo
config :bookapo, ecto_repos: [Bookapo.Repo]


import_config "#{Mix.env()}.exs"
