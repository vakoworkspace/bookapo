use Mix.Config

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

port = String.to_integer(System.get_env("PORT") || "8080")

config :bookapo, Bookapo.Repo,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "15"),
  url: System.get_env("DATABASE_URL"),
  ssl: true
#  username: System.get_env("DATABASE_USER"),
#  password: System.get_env("DATABASE_PASS"),
#  database: System.get_env("DATABASE_NAME"),
#  hostname: System.get_env("DATABASE_HOST")


config :bookapo_admin, BookapoWeb.Endpoint,
  secret_key_base: secret_key_base,
  url: [host: "admin.#{System.get_env("HOSTNAME")}", port: port],
  http: [
    port: port,
    transport_options: [socket_opts: [:inet6]]
  ]

config :bookapo_web, BookapoWeb.Endpoint,
  secret_key_base: secret_key_base,
  url: [host: System.get_env("HOSTNAME"), port: port],
  server: true,
  http: [
    port: port,
    transport_options: [socket_opts: [:inet6]]
  ],
  root: "."
