# Bookapo Umbrella
This is an umbrella app, that currently contains 3 phoenix apps:
  -  Bookapo
  -  BookapoWeb 
  -  BookapoAdmin 
  
Each app has it's own Readme that are available at the root of app's directory.

# Build and Run App
The required tools:
- `docker`
- `docker-compose`
- `make`

1) Change your preferred  settings in `config/docker.env`. (etc: Database credentials and host)

2) Run the following command in the root of umbrella project to create docker image:
```
> make build
```
This command takes a few minutes to finish.

3) Run this command to run you docker container:
```
> make run
```
**Note**: You may need to stop your local postgresql running on port 5432 (`service postgresql stop`) or change db port options on `docker-compose.yml`.

4)  Connect to your database container running at `bookapo_umbrella_db_1`  container:
```
> sudo docker exec -it bookapo_umbrella_db_1 bash 
```
Create your database with the defined credentials at `config/docker.env`:
```
> bash-5.1#  psql -U postgres
psql (13.2)
Type "help" for help.

postgres=# CREATE DATABASE bookapo_bruh;
CREATE DATABASE
```

5) Cancel and Re-run the `make run` command

6) Run your migrations:
```
> sudo docker exec -it bookapo_umbrella_web_1 bash
> bash-5.1#   bin/bookapo_umbrella migrate
```
7) Open `localhost:80`, you should be able to see the app running.

## Creating admins through CLI

1) Connect to your running web container:
```
> sudo docker exec -it bookapo_umbrella_web_1 bash
```
2) Open an elixir console with the following command:
```
bash-5.1#   bin/bookapo_umbrella remote_console
```
3) Create your admin:
```
Bookapo.Accounts.Admin.create(%{username: "exampleuser", password: "123456", password_confirmation: "123456"})
```
