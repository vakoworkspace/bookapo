FROM elixir:1.11.4-alpine AS builder

# install build dependencies
RUN apk update && \
  apk upgrade --no-cache && \
  apk add --no-cache \
    git \
    nodejs \
    python2 \
    yarn --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ \
    build-base && \
  mix local.rebar --force && \
  mix local.hex --force

# The environment to build with
ARG MIX_ENV=prod

# TODO: GENERATE THIS IN RUNTIME
ENV APP_VSN=0.1.1

# prepare build dir
WORKDIR /opt/app

COPY mix.* /opt/app/
COPY apps/bookapo/mix.exs /opt/app/apps/bookapo/mix.exs
COPY apps/bookapo_admin/mix.exs /opt/app/apps/bookapo_admin/mix.exs
COPY apps/bookapo_web/mix.exs /opt/app/apps/bookapo_web/mix.exs

# install mix dependencies
RUN mix do deps.get --only prod, deps.compile

RUN alias python=python2

#COPY apps/bookapo_web/assets/package.json apps/bookapo_web/assets/yarn.lock /opt/app/apps/bookapo_web/assets/
COPY apps/bookapo_web/assets /opt/app/apps/bookapo_web/assets
WORKDIR /opt/app/apps/bookapo_web/assets 
RUN yarn add node-sass@4.14.1
RUN yarn install 
RUN yarn deploy 

RUN apk del yarn
WORKDIR /opt/app
COPY apps/bookapo_web/priv/static/ /opt/app/apps/bookapo_web/priv/static/
COPY apps/ /opt/app/apps
COPY config/ /opt/app/config

RUN cd /opt/app/apps/bookapo_web && \
      mix phx.digest

#COPY apps/bookapo_web/assets/package*.json /opt/app/apps/bookapo_web/assets/
#COPY --from=dependencies /opt/app/deps/phoenix/ /app/deps/phoenix
#COPY --from=dependencies /opt/app/deps/phoenix_html/ /app/deps/phoenix_html

WORKDIR /opt/app
COPY rel /opt/app/rel
RUN mix do compile

RUN \
  mkdir -p /opt/built && \
  mix distillery.release --verbose && \
  cp _build/${MIX_ENV}/rel/bookapo_umbrella/releases/${APP_VSN}/bookapo_umbrella.tar.gz /opt/built && \
  cd /opt/built && \
  tar -xzf bookapo_umbrella.tar.gz && \
  rm bookapo_umbrella.tar.gz

# prepare release image
FROM alpine:3.13 AS app

RUN apk update && \
    apk add --no-cache \
      bash \
      openssl-dev

WORKDIR /opt/app

COPY --from=builder /opt/built .

CMD trap 'exit' INT; /opt/app/bin/bookapo_umbrella foreground
