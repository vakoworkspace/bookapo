.PHONY: help

APP_VSN ?= `grep 'version:' mix.exs | cut -d '"' -f2`

help:
		@echo "$(APP_NAME):$(APP_VSN)-$(BUILD)"
		@perl -nle'print $& if m{^[a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Build the Docker image
		sudo docker build --build-arg APP_VSN=$(APP_VSN) \
        -t bookapo_umbrella:$(APP_VSN) \
        -t bookapo_umbrella:latest .

run: ## Run the app with docker-compose
		sudo docker-compose up
