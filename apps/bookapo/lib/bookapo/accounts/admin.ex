defmodule Bookapo.Accounts.Admin do
  use Ecto.Schema
  use Pow.Ecto.Schema,
    user_id_field: :username,
    password_min_length: 6

  schema "admins" do
    pow_user_fields()

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> pow_changeset(attrs)
    |> Ecto.Changeset.cast(attrs, [:username, :password])
    |> Ecto.Changeset.unique_constraint(:username)
  end

  @doc """
    Used for creating admins through console
  """
  def create(attrs) do
    Pow.Ecto.Context.create(attrs, user: __MODULE__, repo: Bookapo.Repo)
  end
end
