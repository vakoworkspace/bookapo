defmodule Bookapo.Library.Author do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bookapo.Library.Book

  schema "authors" do
    field :about, :string
    field :full_name, :string

    has_many :books, Book

    timestamps()
  end

  @doc false
  def changeset(author, attrs) do
    author
    |> cast(attrs, [:about, :full_name])
    |> validate_required([:about, :full_name])
    |> validate_length(:about, in: 10..100)
  end
end
