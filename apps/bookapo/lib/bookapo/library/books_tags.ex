defmodule Bookapo.Library.BookTag do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bookapo.Library.{Book, Tag}
  
  @primary_key false

  schema "books_tags" do
    belongs_to :book, Book
    belongs_to :tag, Tag
  end

  @doc false
  def changeset(books_tags, attrs) do
    books_tags
    |> cast(attrs, [:book_id, :tag_id])
    |> validate_required([:book_id, :tag_id])
  end
end
