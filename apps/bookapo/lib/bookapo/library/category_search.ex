defmodule Bookapo.Library.CategorySearch do
  @moduledoc """
  The recursive SQL query that loads all subcategories of a given one
  """

  import Ecto.Query, warn: false

  alias Bookapo.Repo
  alias Bookapo.Library.Category

  @query """
    with recursive cat_tree as (
       select id,
              name,
              parent_id
       from categories
       where id = $1  -- this defines the start of the recursion
       union all
       select child.id,
              child.name,
              child.parent_id
       from categories as child
         join cat_tree as parent on parent.id = child.parent_id-- the self join to the CTE builds up the recursion
    )
    select *
    from cat_tree;
    """

  def load_subcategories(id) do
    {:ok, %Postgrex.Result{rows: result}} = Ecto.Adapters.SQL.query(Repo, @query, [id])
    result
    |> Enum.map(fn [id, name, parent_id] -> %Category{id: id, name: name, parent_id: parent_id} end)
  end

  @raw_sql """
  select id, name, parent_id
  from categories
  where id = ANY (?)
  union all
    select child.id, child.name, child.parent_id
  from categories as child
    join cat_tree as parent on parent.id = child.parent_id
  """

  def category_search_query(query, []), do: query 
  def category_search_query(query, category_ids) do
    query
    |> recursive_ctes(true)
    |> with_cte("cat_tree", as: fragment(@raw_sql, ^category_ids))
    |> join(:inner, [b], c in "cat_tree", on: c.id == b.category_id)
  end
end
