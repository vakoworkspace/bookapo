defmodule Bookapo.Library.Search do
  @moduledoc """
  This module converts params map that is passed through action plug to a more human-friendly keyword list
  """

  @filter_key_params ~w(query category_ids author_id sort_option sort_by tags)
  @default_sort_by :inserted_at
  @default_sort_option :desc

  def get_filter_params(params) do
    @filter_key_params
    |> Enum.map(&({String.to_atom(&1), Map.get(params, &1)})) 
    |> Enum.map(fn {key, val} ->
      {key, replace(key, val)}
    end)
  end

  defp replace(:tags, nil), do: []
  defp replace(:tags, tags_string) do
    tags_string
    |> String.replace(", ", ",")
    |> String.split(",")
    |> Enum.filter(fn str -> String.replace(str, " ", "") != "" end) # deletes items that only contain space
    |> Enum.map(&String.trim/1) # "   hello " to "hello"
    |> Enum.map(&String.downcase/1) # cause all of the tagnames in the database are stored in this way
  end

  defp replace(:query, nil), do: ""
  defp replace(:query, q), do: q

  defp replace(:author_id, str) when is_nil(str) or str == "", do: nil
  defp replace(:author_id, str), do: String.to_integer(str)

  defp replace(:sort_by, nil), do: @default_sort_by
  defp replace(:sort_by, sort_by), do: String.to_atom(sort_by)

  defp replace(:sort_option, nil), do: @default_sort_option
  defp replace(:sort_option, sort_option), do: String.to_atom(sort_option)

  defp replace(:category_ids, nil), do: []
  defp replace(:category_ids, list) do
    list
    |> Enum.filter(&(&1!=""))
    |> Enum.map(&String.to_integer(&1))
  end
end
