defmodule Bookapo.Library.Tag do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bookapo.Library.Book

  schema "tags" do
    field :tagname, :string
    many_to_many :books, Book, join_through: "books_tags"
  end

  @doc false
  def changeset(tag, attrs) do
    tag
    |> cast(attrs, [:tagname])
    |> validate_required([:tagname])
  end
end
