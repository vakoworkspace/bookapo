defmodule Bookapo.Library.Category do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bookapo.Library.{Book, Category}

  schema "categories" do
    field :name, :string
    field :parent_id, :integer

    has_many :books, Book
    has_many :subcategories, Category, foreign_key: :parent_id

    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:name, :parent_id])
    |> validate_required([:name])
  end
end
