defmodule Bookapo.Library.Book do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Bookapo.Repo

  alias Bookapo.Library.{Author, Category, Tag}

  schema "books" do
    field :about, :string
    field :english_title, :string
    field :persian_title, :string
    field :release_year, :integer

    belongs_to :author, Author
    belongs_to :category, Category
    many_to_many :tags, Tag,
      join_through: "books_tags",
      on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(book, attrs) do
    book
    |> cast(attrs, [:english_title, :persian_title, :about, :author_id, :category_id, :inserted_at, :release_year])
    |> validate_required([:english_title, :persian_title, :about, :author_id, :category_id, :release_year])
    |> validate_length(:about, in: 10..100)
    |> validate_number(:release_year, less_than_or_equal_to: Date.utc_today.year)
    |> put_assoc(:tags, parse_tags(attrs))
  end

  defp parse_tags(params) do
    (params["tags"] || params[:tags] || "")
    |> String.split(",")
    |> Enum.map(&String.trim/1)
    |> Enum.reject(& &1 == "")
    |> Enum.map(&String.downcase/1)
    |> insert_and_get_all()
  end

  defp insert_and_get_all([]), do: []
  defp insert_and_get_all(names) do
    maps = Enum.map(names, &%{tagname: &1})

    Repo.insert_all Tag, maps, on_conflict: :nothing
    Repo.all from t in Tag, where: t.tagname in ^names
  end
end
