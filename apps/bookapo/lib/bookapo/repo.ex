defmodule Bookapo.Repo do
  use Ecto.Repo,
    otp_app: :bookapo,
    adapter: Ecto.Adapters.Postgres
end
