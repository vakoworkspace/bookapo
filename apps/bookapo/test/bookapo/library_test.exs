defmodule Bookapo.LibraryTest do
  use Bookapo.DataCase

  alias Bookapo.Library

  describe "authors" do
    alias Bookapo.Library.Author

    @valid_attrs %{about: "some about", full_name: "some full_name"}
    @update_attrs %{about: "some updated about", full_name: "some updated full_name"}
    @invalid_attrs %{about: nil, full_name: nil}

    def author_fixture(attrs \\ %{}) do
      {:ok, author} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Library.create_author()

      author
    end

    test "list_authors/0 returns all authors" do
      author = author_fixture()
      assert Library.list_authors() == [author]
    end

    test "get_author!/1 returns the author with given id" do
      author = author_fixture()
      assert Library.get_author!(author.id) == author |> Repo.preload([:books])
      assert Library.get_author!(author.id).books == []
    end

    test "create_author/1 with valid data creates a author" do
      assert {:ok, %Author{} = author} = Library.create_author(@valid_attrs)
      assert author.about == "some about"
      assert author.full_name == "some full_name"
      assert Repo.preload(author, [:books]).books == []
    end

    test "create_author/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Library.create_author(@invalid_attrs)
    end

    test "update_author/2 with valid data updates the author" do
      author = author_fixture()
      assert {:ok, %Author{} = author} = Library.update_author(author, @update_attrs)
      assert author.about == "some updated about"
      assert author.full_name == "some updated full_name"
    end

    test "update_author/2 with invalid data returns error changeset" do
      author = author_fixture()
      assert {:error, %Ecto.Changeset{}} = Library.update_author(author, @invalid_attrs)
      assert author |> Repo.preload([:books]) == Library.get_author!(author.id)
    end

    test "delete_author/1 deletes the author and its books" do
      author = author_fixture()
      category = category_fixture()
      book = book_fixture(%{about: "some about", english_title: "some english_title", persian_title: "some persian_title", author_id: author.id, release_year: 2000, category_id: category.id}) |> Repo.preload([:author, :tags, :category])
      assert Library.list_books == [book]
      assert {:ok, %Author{}} = Library.delete_author(author)
      assert [] = Library.list_books()
      assert_raise Ecto.NoResultsError, fn -> Library.get_author!(author.id) end
    end

    test "change_author/1 returns a author changeset" do
      author = author_fixture()
      assert %Ecto.Changeset{} = Library.change_author(author)
    end
  end

  describe "categories" do
    alias Bookapo.Library.Category

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def category_fixture(attrs \\ %{}) do
      {:ok, category} =
        attrs
        |> Map.merge(@valid_attrs)
        |> Library.create_category()

      category
    end

    test "list_categories/0 returns all categories" do
      category = category_fixture()
      assert Library.list_categories() == [category]
    end

    test "get_category!/1 returns the category with given id" do
      category = category_fixture()
      assert Library.get_category!(category.id) == category |> Repo.preload([:books, :subcategories])
    end

    test "create_category/1 with valid data creates a category" do
      assert {:ok, %Category{} = category} = Library.create_category(@valid_attrs)
      assert category.name == "some name"
    end

    test "create_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Library.create_category(@invalid_attrs)
    end

    test "update_category/2 with valid data updates the category" do
      category = category_fixture()
      assert {:ok, %Category{} = category} = Library.update_category(category, @update_attrs)
      assert category.name == "some updated name"
    end

    test "update_category/2 with invalid data returns error changeset" do
      category = category_fixture()
      assert {:error, %Ecto.Changeset{}} = Library.update_category(category, @invalid_attrs)
      assert category |> Repo.preload([:books, :subcategories]) == Library.get_category!(category.id)
    end

    test "delete_category/1 deletes the category" do
      category = category_fixture()
      assert {:ok, %Category{}} = Library.delete_category(category)
      assert_raise Ecto.NoResultsError, fn -> Library.get_category!(category.id) end
    end

    test "change_category/1 returns a category changeset" do
      category = category_fixture()
      assert %Ecto.Changeset{} = Library.change_category(category)
    end
  end

  describe "books" do
    alias Bookapo.Library.Book
    setup [:create_author, :create_category]

    defp create_author(_), do: %{author_id: author_fixture().id}
    defp create_category(_), do: %{category_id: category_fixture().id}

    @valid_attrs %{about: "some about", english_title: "some english_title", persian_title: "some persian_title", release_year: Date.utc_today.year}
    @update_attrs %{about: "some updated about", english_title: "some updated english_title", persian_title: "some updated persian_title", tags: "tag1, updated_tag2", release_year: Date.utc_today.year - 1}
    @invalid_attrs %{about: nil, english_title: nil, persian_title: nil, release_year: Date.utc_today.year+1}

    def book_fixture(attrs \\ %{}) do
      {:ok, book} = Library.create_book(attrs)
      book |> Repo.preload([:author, :category, :tags])
    end

    test "list_books/0 returns all books", %{author_id: author_id, category_id: category_id} do
      params = %{author_id: author_id, category_id: category_id}
      book = book_fixture(@valid_attrs |> Map.merge(params))
      assert Library.list_books() == [book]
    end

    test "get_book!/1 returns the book with given id", %{author_id: author_id, category_id: category_id} do
      params = %{author_id: author_id, category_id: category_id}
      book = book_fixture(@valid_attrs |> Map.merge(params))
      assert Library.get_book!(book.id) == book |> Repo.preload([:author, :category])
    end

    test "create_book/1 with valid data creates a book", %{author_id: author_id, category_id: category_id} do
      params = %{author_id: author_id, category_id: category_id}
      valid_attrs = Map.merge(@valid_attrs, params)
      assert {:ok, %Book{} = book} = Library.create_book(valid_attrs)
      assert book.about == "some about"
      assert book.english_title == "some english_title"
      assert book.release_year == Date.utc_today.year
      assert book.persian_title == "some persian_title"
    end

    test "create_book/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Library.create_book(@invalid_attrs)
    end

    test "update_book/2 with valid data updates the book", %{author_id: author_id, category_id: category_id} do
      params = %{author_id: author_id, category_id: category_id, tags: "tag1, tag2, tag3"}
      book = book_fixture(@valid_attrs |> Map.merge(params))
      assert {:ok, %Book{} = book} = Library.update_book(book, @update_attrs)
      assert book.about == "some updated about"
      assert book.release_year == Date.utc_today.year - 1
      assert book.english_title == "some updated english_title"
      assert book.persian_title == "some updated persian_title"
      assert book.tags |> Enum.map(&(&1.tagname)) == ["tag1", "updated_tag2"]
      assert Repo.get_by(Library.Tag, tagname: "tag2") |> is_nil()
      assert Repo.get_by(Library.Tag, tagname: "tag3") |> is_nil()
    end

    test "update_book/2 with invalid data returns error changeset", %{author_id: author_id, category_id: category_id} do
      params = %{author_id: author_id, category_id: category_id}
      book = book_fixture(@valid_attrs |> Map.merge(params))
      assert {:error, %Ecto.Changeset{}} = Library.update_book(book, @invalid_attrs)
      assert book == Library.get_book!(book.id)
    end

    test "delete_book/1 deletes the book", %{author_id: author_id, category_id: category_id} do
      params = %{author_id: author_id, category_id: category_id}
      book = book_fixture(@valid_attrs |> Map.merge(params))
      assert {:ok, %Book{}} = Library.delete_book(book)
      assert_raise Ecto.NoResultsError, fn -> Library.get_book!(book.id) end
    end

    test "change_book/1 returns a book changeset", %{author_id: author_id, category_id: category_id} do
      params = %{author_id: author_id, category_id: category_id}
      book = book_fixture(@valid_attrs |> Map.merge(params)) 
      assert {:ok, %Book{}} = Library.delete_book(book)
      assert %Ecto.Changeset{} = Library.change_book(book)
    end
  end

  describe "tags" do
    alias Bookapo.Library.Tag

    @valid_attrs %{tagname: "some tagname"}
    @update_attrs %{tagname: "some updated tagname"}
    @invalid_attrs %{tagname: nil}

    def tag_fixture(attrs \\ %{}) do
      {:ok, tag} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Library.create_tag()

      tag
    end

    test "list_tags/0 returns all tags" do
      tag = tag_fixture()
      assert Library.list_tags() == [tag]
    end

    test "get_tag!/1 returns the tag with given id" do
      tag = tag_fixture()
      assert Library.get_tag!(tag.id) == tag
    end

    test "create_tag/1 with valid data creates a tag" do
      assert {:ok, %Tag{} = tag} = Library.create_tag(@valid_attrs)
      assert tag.tagname == "some tagname"
    end

    test "create_tag/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Library.create_tag(@invalid_attrs)
    end

    test "update_tag/2 with valid data updates the tag" do
      tag = tag_fixture()
      assert {:ok, %Tag{} = tag} = Library.update_tag(tag, @update_attrs)
      assert tag.tagname == "some updated tagname"
    end

    test "update_tag/2 with invalid data returns error changeset" do
      tag = tag_fixture()
      assert {:error, %Ecto.Changeset{}} = Library.update_tag(tag, @invalid_attrs)
      assert tag == Library.get_tag!(tag.id)
    end

    test "delete_tag/1 deletes the tag" do
      tag = tag_fixture()
      assert {:ok, %Tag{}} = Library.delete_tag(tag)
      assert_raise Ecto.NoResultsError, fn -> Library.get_tag!(tag.id) end
    end

    test "change_tag/1 returns a tag changeset" do
      tag = tag_fixture()
      assert %Ecto.Changeset{} = Library.change_tag(tag)
    end
  end
end
