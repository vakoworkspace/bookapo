defmodule Bookapo.SearchTest do
  use Bookapo.DataCase

  alias Bookapo.Library.Search

  @nil_map %{}
  @nil_keyword [
    query: "", 
    category_ids: [],
    author_id: nil,
    sort_option: :desc,
    sort_by: :inserted_at,
    tags: [],
  ]

  @half_filled_params %{
    "query" => "",
    "category_ids" => ["","","",""],
    "author_id" => "",
    "sort_option" => nil,
    "sort_by" => nil,
    "tags" => "",
    "invalid_field" => :invalid_data
  }

  @full_filled_params %{
    "query" => "some query",
    "category_ids" => ["","3","","34"],
    "author_id" => "2",
    "sort_option" => "asc",
    "sort_by" => "inserted_at",
    "tags" => "hello,elixir,some tag, fun"
  }

  describe "get_filter_params" do
    test "empty map" do
      assert Search.get_filter_params(@nil_map) == @nil_keyword
    end

    test "half filled params" do
      half_filled_keyword_list = Search.get_filter_params(@half_filled_params)
      assert half_filled_keyword_list == [
        query: "",
        category_ids: [],
        author_id: nil,
        sort_option: :desc,
        sort_by: :inserted_at,
        tags: []
      ]
    end

    test "full filled params" do
      full_filled_keyword_list = Search.get_filter_params(@full_filled_params) 
      assert full_filled_keyword_list == [
        query: "some query",
        category_ids: [3, 34],
        author_id: 2,
        sort_option: :asc,
        sort_by: :inserted_at,
        tags: ["hello", "elixir", "some tag", "fun"]
      ]
    end
  end
end
