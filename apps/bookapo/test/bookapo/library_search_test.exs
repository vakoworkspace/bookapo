defmodule Bookapo.LibrarySearchTest do
  use Bookapo.DataCase

  alias Bookapo.Library

  @author_params [
    %{about: "some about author 1", full_name: "author 1", inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second), updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)},
    %{about: "some about author 2", full_name: "author 2", inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second), updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)}
  ]

  @category_params [
    %{name: "comdey", inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second), updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)},
    %{name: "tech", inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second), updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)}
  ]

  setup :generate_books

  defp get_authors, do: Repo.all(Library.Author)
  defp get_categories, do: Repo.all(Library.Category)

  defp get_book_params() do
    authors = get_authors()
    categories = get_categories()

    do_loop_on_book_params([], 1, authors, categories)
  end

  defp do_loop_on_book_params(params, 13, _authors, _categories), do: params
  defp do_loop_on_book_params(params, i, authors, categories) do
    n = rem(i-1, 3)+1
    a = cond do
      i < 7 -> 0
      true -> 1
    end
    map = %{
      about: "some about #{n}",
      persian_title: "some persian title #{n}",
      english_title: "some english title #{n}",
      author_id: Enum.at(authors, a).id,
      category_id: Enum.at(categories, rem(i+1, 2)).id,
      updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second),
      inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.add(i) |> NaiveDateTime.truncate(:second),
      release_year: 2000+i
    }

    do_loop_on_book_params(params ++ [map], i+1, authors, categories)
  end


  defp generate_books(_) do
    Repo.insert_all(Library.Category, @category_params)
    Repo.insert_all(Library.Author, @author_params)
    Repo.insert_all(Library.Book, get_book_params())

    [
      books: Repo.all(from b in Library.Book, order_by: [desc: :inserted_at]) 
             |> Repo.preload([:tags, :category, :author])
    ]
  end

  @nil_keyword [
    query: "", 
    category_ids: [],
    author_id: nil,
    sort_option: :desc,
    sort_by: :inserted_at,
    tags: [],
  ]

  test "list_books/1 with @nil_keyword should return all of the books", %{books: books} do
    assert Library.list_books(@nil_keyword) == books
  end

  describe "query" do

    test "list_books/1 with query", %{books: books} do
      books = Enum.filter(books, fn book -> 
        String.contains?(book.english_title, "1") or
        String.contains?(book.persian_title, "1") or
        String.contains?(book.author.full_name, "1") 
      end)
      new_keyword = @nil_keyword
                    |> Keyword.replace(:query, "1") 
  
      assert Library.list_books(new_keyword) == books
    end

    test "list_books/1 with invalid query", %{books: _} do
      new_keyword = @nil_keyword
                    |> Keyword.replace(:query, "some invalid query")

      assert Library.list_books(new_keyword) == []
    end
  end

  describe "category_ids" do

    test "list_books/1 with category_ids", %{books: books} do
      new_keyword = @nil_keyword
                    |> Keyword.replace(:category_ids, Enum.map(books, &(&1.category_id)))

      assert Library.list_books(new_keyword) == books
    end
  end

  describe "author_id" do
    test "list_books/1 with author_id", %{books: [book|_other_books] = books} do
      books = Enum.filter(books, fn b ->
        b.author_id == book.author_id
      end)
      new_keyword = @nil_keyword
                    |> Keyword.replace(:author_id, book.author_id)

      assert Library.list_books(new_keyword) == books
    end
  end

  describe "tags" do
    test "list_books/1 with tags", %{books: books} do
      books = Enum.filter(books, fn book ->
        Enum.map(book.tags, &(&1.tagname)) |> Enum.any?(&(&1 in ["tag3", "tag1"]))
      end)
      new_keyword = @nil_keyword
                    |> Keyword.replace(:tags, ["tag3", "tag1"])

      assert Library.list_books(new_keyword) == books
    end
  end

  describe "sort" do
    test "list_books/1 with release_year", %{books: books} do
      new_keyword = @nil_keyword
                    |> Keyword.replace(:sort_by, :release_year)

      assert Library.list_books(new_keyword) == Enum.sort(books, &(&1.release_year > &2.release_year))
    end

    test "list_books/1 with recently_added", %{books: books} do
      new_keyword = @nil_keyword
                    |> Keyword.replace(:sort_by, :inserted_at)
                    |> Keyword.replace(:sort_option,:asc)

      assert Library.list_books(new_keyword) == Enum.reverse(books)
    end
  end
end
