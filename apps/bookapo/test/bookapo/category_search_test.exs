defmodule Bookapo.CategorySearchTest do
  use Bookapo.DataCase

  alias Bookapo.Library
  alias Bookapo.Library.{CategorySearch, Book}

  import Ecto.Query

  setup :insert_categories_and_books

  def get_category_params(num, parent_id), do: %Library.Category{name: "category #{num}", parent_id: parent_id}
  def get_book_params(num, category_id, author_id) do
    %Library.Book{
      english_title: "book #{num}",
      category_id: category_id,
      persian_title: "ketab #{num}",
      about: "some about #{num}",
      author_id: author_id,
      release_year: 2000+num,
      tags: []
    }
  end

  @author_attrs %Library.Author{about: "some about", full_name: "some full_name"}

  def insert_categories_and_books(_) do
    # c1 -> b1
    # c2
    #   c3 -> b2
    #     c5 -> b3
    #   c4 -> b4, b5
    # c6
    {:ok, c1} = Repo.insert(get_category_params(1, nil))
    {:ok, c2} = Repo.insert(get_category_params(2, nil))
    {:ok, c3} = Repo.insert(get_category_params(3, c2.id))
    {:ok, c4} = Repo.insert(get_category_params(4, c2.id))
    {:ok, c5} = Repo.insert(get_category_params(5, c3.id))
    {:ok, c6} = Repo.insert(get_category_params(6, nil))

    {:ok, author} = Repo.insert(@author_attrs)

    {:ok, b1} = Repo.insert(get_book_params(1, c1.id, author.id))
    {:ok, b2} = Repo.insert(get_book_params(2, c3.id, author.id))
    {:ok, b3} = Repo.insert(get_book_params(3, c5.id, author.id))
    {:ok, b4} = Repo.insert(get_book_params(4, c4.id, author.id))
    {:ok, b5} = Repo.insert(get_book_params(5, c4.id, author.id))

    [
      categories: [c1, c2, c3, c4, c5, c6],
      books: [b1, b2, b3, b4, b5]
    ]
  end

  describe "load_subcategories/1" do

    test "should return the given category when no childs", %{categories: [_c1,_c2,_c3,_c4,c5,_c6]} do
      assert CategorySearch.load_subcategories(c5.id) |> Enum.map(&(&1.id)) == [c5.id]
    end

    test "should return the childs", %{categories: [_c1,c2,c3,c4,c5,_c6]} do
      assert CategorySearch.load_subcategories(c2.id) |> Enum.map(&(&1.id)) |> Enum.sort() == [c2.id, c3.id, c4.id, c5.id] |> Enum.sort()
    end
  end

  describe "category_search_query/2" do

    test "should return the query when empty list given" do
      query = from(b in Book)
      assert CategorySearch.category_search_query(query, []) == query
    end

    test "Should return children's books", %{categories: [c1,c2,c3,c4,c5,c6], books: [b1,b2,b3,b4,b5] = books} do
      query = from(b in Book)
      assert CategorySearch.category_search_query(query, [c1.id]) |> get_result_ids() == [b1.id]
      assert CategorySearch.category_search_query(query, [c2.id]) |> get_result_ids() == [b2.id, b3.id, b4.id, b5.id] |> Enum.sort()
      assert CategorySearch.category_search_query(query, [c5.id]) |> get_result_ids() == [b3.id] |> Enum.sort()
      assert CategorySearch.category_search_query(query, [c6.id]) |> get_result_ids() == []
      assert CategorySearch.category_search_query(query, [c1.id, c2.id]) |> get_result_ids() == books |> Enum.map(&(&1.id)) |> Enum.sort
      assert CategorySearch.category_search_query(query, [c4.id, c5.id]) |> get_result_ids() == [b3.id, b4.id, b5.id] |> Enum.sort
    end
  end

    # c1 -> b1
    # c2
    #   c3 -> b2
    #     c5 -> b3
    #   c4 -> b4, b5
    # c6
    
  defp get_result_ids(query) do
    query
    |> Bookapo.Repo.all()
    |> Enum.map(&(&1.id))
    |> Enum.sort()
  end
end

