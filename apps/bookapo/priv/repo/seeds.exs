# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Bookapo.Repo.insert!(%Bookapo.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
# 10 authors and 10 categories
url = "https://jsonplaceholder.ir/posts"
response = HTTPoison.get!(url)
response_body = Poison.decode!(response.body)

categories_and_tags = %{
  "زندگی‌نامه" => ~w(زندگی‌نامه بیوگرافی مشاهیر),
  "فلسفی" => ~w(فلسفی روانشناسی سبک زندگی ),
  "داستان" => ~w(رمان داستان افسانه),
  "آموزشی" => ~w(آموزشی بیزینس),
  "تکنولوژی" => ~w(برنامه‌نویسی تعمیرات),
  "علمی" => ~w(ریاضیات فیزیک ادبیات)
}

category_params = Enum.map(categories_and_tags, fn {k, _v} -> 
  %{
    name: k,
    parent_id: nil,
    inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second),
    updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
  }
end) 

author_params = [
  %{
    full_name: "ویلیام شکسپیر",
    about: "شکسپیر را پدر نمایشنامه نویسیی انگلستان می‌دانند. وی موضوع بسیاری از نمایشنامه‌های خویش را از تاریخ روم باستان و کتاب پلوتارک برگزیده و به نیروی تصور و تخیل خویش به صورت تراژدی‌های زیبایی آن‌ها را درآورده است. سبک نویسندگی شکسپیر به مکتب کلاسیسم تعلق دارد. نمایشنامه‌های وی را به تراژدی و کمدی و نمایشنامه‌های تاریخی تقسیم می‌کنند. نمایشنامه‌های اخیر وی ترکیبی از تراژدی و کمدی است. آثار مهم او عبارت است از :اتللو، مکبث، هملت، جولیوس قیصر، تاجر ونیزی، شاه لیر، هنری ششم، هیاهوی بسیار برای هیچ، رویای شب نیمه تابستان. ",
    inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second),
    updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
  },
  %{
    full_name: "استیون کینگ",
    about: "استیون کینگ، ۷۲ ساله، متولد سپتامبر ۱۹۴۷، که به رمان‌هایش در ژانر وحشت شهرت دارد، نویسنده‌ی امریکایی است که تم‌های فانتزی، سوپر نچرال و رازآلودگی را می‌توان در رمان‌هایش دید. او در سال ۱۹۶۷ به صورت حرفه‌ای نوشتن داستان کوتاه را آغاز کرد و از طریق آن کسب درآمد می‌کرد و بعد در سال ۱۹۷۴ اولین رمان خود به نام «کری» را منتشر کرد که رمانی در ژانر وحشت بود.",
    inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second),
    updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
  },
  %{
    full_name: "هاروکی موراکامی",
    about: "هاروکی موراکامی، ۷۰ ساله، متولد ژانویه ۱۳۴۹، نویسنده‌ی ژاپنی که از همان سال‌های آغازین کار حرفه‌ای در خارج از ژاپن شناخته شد امروزه یکی از مشهورترین نویسندگان معاصر است. او در ۲۹ سالگی نوشتن را آغاز کرد و به گفته‌ی خودش پیش از آن یک کلوپ جز را اداره می‌کرد و هیچ چیزی خلق نکرده بود. ",
    inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second),
    updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
  }
]

Bookapo.Repo.insert_all(Bookapo.Library.Category, category_params)
Bookapo.Repo.insert_all(Bookapo.Library.Author, author_params)

book_params = Enum.map(response_body, fn resource ->
  category = Bookapo.Library.list_categories() |> Enum.at(:rand.uniform(5))
  author = Bookapo.Library.list_authors() |> Enum.at(:rand.uniform(2))
  tags = categories_and_tags[category.name] 
         |> Enum.shuffle() 
         |> Enum.slice(0..(:rand.uniform(2)-1))
         |> Enum.join(", ")
  %{
    about: resource["body"],
    persian_title: resource["title"],
    english_title: "ENGLISH TITLE LOL",
    inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second),
    updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second),
    release_year: :rand.uniform(100)+Date.utc_today.year-100, # last century
    category_id: category.id,
    tags: tags,
    author_id: author.id
  }
end)

for b_p <- book_params do
  Bookapo.Library.create_book(b_p)
end

#for a <- 1..5 do 
#  author_params = %Bookapo.Library.Author{
#    full_name: Faker.Person.name(),
#    about: Faker.Lorem.paragraph(2..4)
#  }
#  author = Bookapo.Repo.insert!(author_params)
#  for c <- 1..5 do 
#    category = if a == 1 do
#      category_params = %Bookapo.Library.Category{
#        name: Faker.Lorem.sentence(2..5)
#      }
#      Bookapo.Repo.insert!(category_params)
#    else
#      Bookapo.Library.list_books |> Enum.at(c-1) |> Map.get(:category)
#    end
#
#    tags_string = Faker.Lorem.sentence(0..5) 
#                  |> String.replace(" ", ",") 
#
#    book_params = %{
#      about: Faker.Lorem.paragraph(2..4),
#      english_title: Faker.Lorem.sentence(2..5),
#      persian_title: "فیکر فارسی نداشت ",
#      author_id: author.id,
#      category_id: category.id,
#      tags: tags_string |> String.slice(0..(String.length(tags_string)-2)),
#      release_year: :rand.uniform(Date.utc_today.year)
#    }
#
#    {:ok, _} = Bookapo.Library.create_book(book_params)
#  end
#end
