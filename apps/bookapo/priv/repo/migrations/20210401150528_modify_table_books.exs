defmodule Bookapo.Repo.Migrations.ModifyTableBooks do
  use Ecto.Migration

  def change do
    alter table(:books) do
      remove :about
      add :about, :text, null: false
    end
  end
end
