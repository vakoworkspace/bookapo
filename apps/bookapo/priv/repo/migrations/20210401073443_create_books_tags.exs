defmodule Bookapo.Repo.Migrations.CreateBooksTags do
  use Ecto.Migration

  def change do
    create table(:books_tags) do
      add :book_id, references(:books, on_delete: :delete_all), null: false
      add :tag_id, references(:tags, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:books_tags, [:book_id])
    create index(:books_tags, [:tag_id])
    create unique_index(:books_tags, [:book_id, :tag_id])
  end
end
