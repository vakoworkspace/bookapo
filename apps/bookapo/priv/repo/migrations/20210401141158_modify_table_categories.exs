defmodule Bookapo.Repo.Migrations.ModifyTableCategories do
  use Ecto.Migration

  def change do
    drop unique_index(:categories, [:name])
  end
end
