defmodule Bookapo.Repo.Migrations.CreateBooks do
  use Ecto.Migration

  def change do
    create table(:books) do
      add :about, :text, null: false
      add :english_title, :string, null: false
      add :persian_title, :string, null: false
      add :release_year, :integer, null: false
      add :author_id, references(:authors, on_delete: :delete_all)
      add :category_id, references(:categories, on_delete: :nothing)

      timestamps()
    end

    create index(:books, [:author_id])
    create index(:books, [:category_id])
  end
end
