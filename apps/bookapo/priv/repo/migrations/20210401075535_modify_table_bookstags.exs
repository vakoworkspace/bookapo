defmodule Bookapo.Repo.Migrations.ModifyTableBookstags do
  use Ecto.Migration

  def change do
    alter table(:books_tags) do
      remove :inserted_at
      remove :updated_at
    end
  end
end
