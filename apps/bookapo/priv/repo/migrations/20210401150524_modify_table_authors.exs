defmodule Bookapo.Repo.Migrations.ModifyTableAuthors do
  use Ecto.Migration

  def change do
    alter table(:authors) do
      remove :about
      add :about, :text, null: false
    end
  end
end
