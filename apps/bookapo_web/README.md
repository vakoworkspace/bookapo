# BookapoWeb

Business logic of the application (etc: buying subscriptions) is done through BookapoWeb endpoint.

Requests are routed to the right endpoint based on their subdomin in the `BookapoWeb.ProxyPlug` module plug. This mean that for starting your server, you need to run `mix phx.server` in the root of this app.
