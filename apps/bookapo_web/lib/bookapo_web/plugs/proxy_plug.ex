defmodule BookapoWeb.ProxyPlug do
  import Plug.Conn

  @doc false
  def init(default), do: default

  @doc false
  def call(conn, _router) do
    cond do
      String.match?(conn.request_path , ~r/^\/admin/) -> 
        conn
        |> BookapoAdminWeb.Endpoint.call([])
        |> halt

      true -> conn
    end
  end
end
