defmodule BookapoWeb.BookController do
  use BookapoWeb, :controller

  alias Bookapo.Library
  alias Bookapo.Library.Search

  def index(conn, params) do
    books = Library.list_books(Search.get_filter_params(params))
    categories = Library.list_categories()
    authors = Library.list_authors()
    render(conn, "index.html", books: books, categories: categories, authors: authors)
  end

  def show(conn, %{"id" => id}) do
    book = Library.get_book!(id)
    render(conn, "show.html", book: book)
  end
end
