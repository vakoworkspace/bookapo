defmodule BookapoWeb.PageController do
  use BookapoWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
