defmodule BookapoWeb.CategoryController do
  use BookapoWeb, :controller

  alias Bookapo.Library

  def show(conn, %{"id" => id}) do
    {id, _} = Integer.parse(id)
    categories = Library.CategorySearch.load_subcategories(id)
    [category | subcategories] = categories
    books = Library.list_books(category_ids: Enum.map(categories, &(&1.id)))
    render(conn, "show.html", category: category, subcategories: subcategories, books: books)
  end
end
