defmodule BookapoWeb.AuthorController do
  use BookapoWeb, :controller

  alias Bookapo.Library

  def show(conn, %{"id" => id}) do
    author = Library.get_author!(id)
    render(conn, "show.html", author: author)
  end
end
