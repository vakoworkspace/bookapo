defmodule BookapoWeb.BookControllerTest do
  use BookapoWeb.ConnCase

  alias Bookapo.Repo
  alias Bookapo.Library

  def create_attrs(author, category) do
    %{
      about: "some about", 
      english_title: "some english_title", 
      persian_title: "some persian_title",
      category_id: category.id,
      author_id: author.id,
      tags: "tag1, tag2, tag3",
      release_year: Date.utc_today.year
    }
  end

  @author_create_attrs %{about: "some about", full_name: "some full_name"}
  @category_create_attrs %{name: "some name"}

  def fixture(:book) do
    {:ok, book} = Library.create_book(create_attrs(fixture(:author), fixture(:category)))
    book |> Repo.preload(:tags)
  end

  def fixture(:category) do
    {:ok, category} = Library.create_category(@category_create_attrs)
    category
  end

  def fixture(:author) do
    {:ok, author} = Library.create_author(@author_create_attrs)
    author
  end

  describe "index books" do
    test "lists all books", %{conn: conn} do
      conn = get(conn, Routes.book_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Books"
    end
  end

  describe "show book" do
    setup [:create_book]

    test "shows book", %{conn: conn, book: book} do
      conn = get(conn, Routes.book_path(conn, :show, book))
      assert html_response(conn, 200) =~ "some english_title"
    end
  end

  defp create_book(_) do
    book = fixture(:book)
    %{book: book |> Repo.preload([:author, :category])}
  end
end
