defmodule BookapoWeb.CategoryControllerTest do
  use BookapoWeb.ConnCase

  alias Bookapo.Library

  @create_attrs %{name: "some name"}

  def fixture(:category) do
    {:ok, category} = Library.create_category(@create_attrs)
    category
  end

  describe "show category" do
    setup [:create_category]

    test "shows category", %{conn: conn, category: category} do
      conn = get(conn, Routes.category_path(conn, :show, category))
      assert html_response(conn, 200) =~ "some name"
    end
  end

  defp create_category(_) do
    category = fixture(:category)
    %{category: category}
  end
end
