defmodule BookapoWeb.AuthorControllerTest do
  use BookapoWeb.ConnCase

  alias Bookapo.Library

  @create_attrs %{about: "some about", full_name: "some full_name"}

  def fixture(:author) do
    {:ok, author} = Library.create_author(@create_attrs)
    author
  end

  describe "show author" do
    setup [:create_author]

    test "shows author", %{conn: conn, author: author} do
      conn = get(conn, Routes.author_path(conn, :show, author))
      assert html_response(conn, 200) =~ "some about"
    end
  end

  defp create_author(_) do
    author = fixture(:author)
    %{author: author}
  end
end
