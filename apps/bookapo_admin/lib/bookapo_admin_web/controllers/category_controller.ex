defmodule BookapoAdminWeb.CategoryController do
  use BookapoAdminWeb, :controller

  alias Bookapo.Library
  alias Bookapo.Library.Category

  def new(conn, _params) do
    categories = Library.list_categories()
    changeset = Library.change_category(%Category{})
    render(conn, "new.html", changeset: changeset, categories: categories)
  end

  def create(conn, %{"category" => category_params}) do
    case Library.create_category(category_params) do
      {:ok, _category} ->
        conn
        |> put_flash(:info, "Category created successfully.")
        |> redirect(to: Routes.admin_path(conn, :show))

      {:error, %Ecto.Changeset{} = changeset} ->
        categories = Library.list_categories()
        render(conn, "new.html", changeset: changeset, categories: categories)
    end
  end

  def edit(conn, %{"id" => id}) do
    categories = Library.list_categories()
    category = Library.get_category!(id)
    changeset = Library.change_category(category)
    render(conn, "edit.html", changeset: changeset, categories: categories, category: category)
  end

  def update(conn, %{"id" => id, "category" => category_params}) do
    category = Library.get_category!(id)

    case Library.update_category(category, category_params) do
      {:ok, _category} ->
        conn
        |> put_flash(:info, "Category updated successfully.")
        |> redirect(to: Routes.admin_path(conn, :show))

      {:error, %Ecto.Changeset{} = changeset} ->
        categories = Library.list_categories()
        render(conn, "edit.html", category: category, changeset: changeset, categories: categories)
    end
  end

  def delete(conn, %{"id" => id}) do
    category = Library.get_category!(id)
    {:ok, _category} = Library.delete_category(category)

    conn
    |> put_flash(:info, "Category deleted successfully.")
    |> redirect(to: Routes.admin_path(conn, :show))
  end
end
