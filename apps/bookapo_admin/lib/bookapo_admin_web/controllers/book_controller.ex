defmodule BookapoAdminWeb.BookController do
  use BookapoAdminWeb, :controller

  alias Bookapo.Library
  alias Bookapo.Library.Book

  def new(conn, _params) do
    changeset = Library.change_book(%Book{})
    authors = Library.list_authors()
    categories = Library.list_categories()
    render(conn, "new.html", changeset: changeset, authors: authors, categories: categories)
  end

  def create(conn, %{"book" => book_params}) do
    case Library.create_book(book_params) do
      {:ok, _book} ->
        conn
        |> put_flash(:info, "Book created successfully.")
        |> redirect(to: Routes.admin_path(conn, :show))

      {:error, %Ecto.Changeset{} = changeset} ->
        authors = Library.list_authors()
        categories = Library.list_categories()
        render(conn, "new.html", changeset: changeset, authors: authors, categories: categories)
    end
  end

  def edit(conn, %{"id" => id}) do
    book = Library.get_book!(id)
    changeset = Library.change_book(book)
    authors = Library.list_authors()
    categories = Library.list_categories()
    render(conn, "edit.html", book: book, changeset: changeset, authors: authors, categories: categories)
  end

  def update(conn, %{"id" => id, "book" => book_params}) do
    book = Library.get_book!(id)

    case Library.update_book(book, book_params) do
      {:ok, _book} ->
        conn
        |> put_flash(:info, "Book updated successfully.")
        |> redirect(to: Routes.admin_path(conn, :show))

      {:error, %Ecto.Changeset{} = changeset} ->
        authors = Library.list_authors()
        categories = Library.list_categories()
        render(conn, "edit.html", book: book, changeset: changeset, authors: authors, categories: categories)
    end
  end

  def delete(conn, %{"id" => id}) do
    book = Library.get_book!(id)
    {:ok, _book} = Library.delete_book(book)

    conn
    |> put_flash(:info, "Book deleted successfully.")
    |> redirect(to: Routes.admin_path(conn, :show))
  end
end
