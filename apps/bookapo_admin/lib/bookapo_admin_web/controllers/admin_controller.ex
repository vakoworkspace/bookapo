defmodule BookapoAdminWeb.AdminController do
  use BookapoAdminWeb, :controller

  def show(conn, _) do
    current_admin = Pow.Plug.current_user(conn)
    render(conn, "show.html", current_admin: current_admin)
  end
end
