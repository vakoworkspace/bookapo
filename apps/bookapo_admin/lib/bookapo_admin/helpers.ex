defmodule BookapoAdmin.Helpers do
  def admins_signed_in?(conn) do
    !!(
    conn
    |> Pow.Plug.put_config(repo: Bookapo.Repo, user: Bookapo.Accounts.Admin)
    |> Pow.Plug.current_user()
    )
  end
end
