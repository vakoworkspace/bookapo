defmodule BookapoAdminWeb.AuthorControllerTest do
  use BookapoAdminWeb.ConnCase

  alias Bookapo.Library
  alias Bookapo.Accounts.Admin

  @create_attrs %{about: "some about", full_name: "some full_name"}
  @update_attrs %{about: "some updated about", full_name: "some updated full_name"}
  @invalid_attrs %{about: nil, full_name: nil}

  def fixture(:author) do
    {:ok, author} = Library.create_author(@create_attrs)
    author
  end

  setup %{conn: conn} do
    admin = %Admin{username: "testexample"}
    conn = Pow.Plug.assign_current_user(conn, admin, otp_app: :bookapo)

    {:ok, protected_conn: conn}
  end

  defp expected_redirect_conn(conn, :with_request_path), do: Routes.pow_session_path(conn, :new, request_path: Phoenix.Controller.current_path(conn))
  defp expected_redirect_conn(conn, :no_request_path), do: Routes.pow_session_path(conn, :new)

  describe "new author" do
    test "renders form if admin signed in", %{protected_conn: conn} do
      conn = get(conn, Routes.author_path(conn, :new))
      assert html_response(conn, 200) =~ "New Author"
    end

    test "redirect to login page if admin haven't signed in", %{conn: conn} do
      conn = get(conn, Routes.author_path(conn, :new))
      assert redirected_to(conn) == expected_redirect_conn(conn, :with_request_path)
    end
  end

  describe "create author" do
    test "redirects to login when no admin signed in", %{conn: conn} do
      conn = post(conn, Routes.author_path(conn, :create), author: @create_attrs)
      assert redirected_to(conn) == expected_redirect_conn(conn, :no_request_path)
    end

    test "redirects to show when data is valid", %{protected_conn: conn} do
      conn = post(conn, Routes.author_path(conn, :create), author: @create_attrs)
      assert redirected_to(conn) == Routes.admin_path(conn, :show)
    end

    test "renders errors when data is invalid", %{protected_conn: conn} do
      conn = post(conn, Routes.author_path(conn, :create), author: @invalid_attrs)
      assert html_response(conn, 200) =~ "wrong"
    end
  end

  describe "edit author" do
    setup [:create_author]

    test "redirects to login when no admin signed in", %{conn: conn, author: author} do
      conn = get(conn, Routes.author_path(conn, :edit, author))
      assert redirected_to(conn) == expected_redirect_conn(conn, :with_request_path)
    end

    test "renders form for editing chosen author", %{protected_conn: conn, author: author} do
      conn = get(conn, Routes.author_path(conn, :edit, author))
      assert html_response(conn, 200) =~ "Edit Author"
    end
  end

  describe "update author" do
    setup [:create_author]

    test "redirects to login page if no admin signed in", %{conn: conn, author: author} do
      conn = put(conn, Routes.author_path(conn, :update, author), author: @update_attrs)
      assert redirected_to(conn) == expected_redirect_conn(conn, :no_request_path)
    end

    test "redirects when data is valid", %{protected_conn: conn, author: author} do
      conn = put(conn, Routes.author_path(conn, :update, author), author: @update_attrs)
      assert redirected_to(conn) == Routes.admin_path(conn, :show)
    end

    test "renders errors when data is invalid", %{protected_conn: conn, author: author} do
      conn = put(conn, Routes.author_path(conn, :update, author), author: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Author"
    end
  end

  describe "delete author" do
    setup [:create_author]

    test "redirects to login when no admin signed in", %{conn: conn, author: author} do
      conn = delete(conn, Routes.author_path(conn, :delete, author))
      assert redirected_to(conn) == expected_redirect_conn(conn, :no_request_path)
    end

    test "deletes chosen author", %{protected_conn: conn, author: author} do
      conn = delete(conn, Routes.author_path(conn, :delete, author))
      assert redirected_to(conn) == Routes.admin_path(conn, :show)
    end
  end

  defp create_author(_) do
    author = fixture(:author)
    %{author: author}
  end
end
