defmodule BookapoAdminWeb.BookControllerTest do
  use BookapoAdminWeb.ConnCase

  alias Bookapo.Repo
  alias Bookapo.Library
  alias Bookapo.Accounts.Admin

  setup %{conn: conn} do
    admin = %Admin{username: "testexample"}
    conn = Pow.Plug.assign_current_user(conn, admin, otp_app: :bookapo)

    {:ok, protected_conn: conn}
  end

  defp expected_redirect_conn(conn, :with_request_path), do: Routes.pow_session_path(conn, :new, request_path: Phoenix.Controller.current_path(conn))
  defp expected_redirect_conn(conn, :no_request_path), do: Routes.pow_session_path(conn, :new)

  def create_attrs(author, category) do
    %{
      about: "some about", 
      english_title: "some english_title", 
      persian_title: "some persian_title",
      category_id: category.id,
      author_id: author.id,
      tags: "tag1, tag2, tag3",
      release_year: Date.utc_today.year
    }
  end

  def update_attrs(author, category) do
    %{
      about: "some updated about", 
      english_title: "some updated english_title", 
      persian_title: "some updated persian_title",
      category_id: category.id,
      author_id: author.id,
      tags: "tag1, updated_tag2",
      release_year: Date.utc_today.year - 1
    }
  end

  @invalid_attrs %{about: nil, english_title: nil, persian_title: nil}
  @author_create_attrs %{about: "some about", full_name: "some full_name"}
  @category_create_attrs %{name: "some name"}

  def fixture(:book) do
    {:ok, book} = Library.create_book(create_attrs(fixture(:author), fixture(:category)))
    book |> Repo.preload(:tags)
  end

  def fixture(:category) do
    {:ok, category} = Library.create_category(@category_create_attrs)
    category
  end

  def fixture(:author) do
    {:ok, author} = Library.create_author(@author_create_attrs)
    author
  end

  describe "new book" do
    test "redirects when no admin signed in", %{conn: conn} do
      conn = get(conn, Routes.book_path(conn, :new))
      assert redirected_to(conn) == expected_redirect_conn(conn, :with_request_path)
    end

    test "renders form", %{protected_conn: conn} do
      conn = get(conn, Routes.book_path(conn, :new))
      assert html_response(conn, 200) =~ "New Book"
    end
  end

  describe "create book" do
    test "redirects to login when no admin signed in", %{conn: conn} do
      conn = post(conn, Routes.book_path(conn, :create), book: create_attrs(fixture(:author), fixture(:category)))
      assert redirected_to(conn) == expected_redirect_conn(conn, :no_request_path)
    end

    test "redirects to show when data is valid", %{protected_conn: conn} do
      conn = post(conn, Routes.book_path(conn, :create), book: create_attrs(fixture(:author), fixture(:category)))
      assert redirected_to(conn) == Routes.admin_path(conn, :show)
    end

    test "renders errors when data is invalid", %{protected_conn: conn} do
      conn = post(conn, Routes.book_path(conn, :create), book: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Book"
    end
  end

  describe "edit book" do
    setup [:create_book]

    test "redirects to login when no admin signed in", %{conn: conn, book: book} do
      conn = get(conn, Routes.book_path(conn, :edit, book))
      assert redirected_to(conn) == expected_redirect_conn(conn, :with_request_path)
    end

    test "renders form for editing chosen book", %{protected_conn: conn, book: book} do
      conn = get(conn, Routes.book_path(conn, :edit, book))
      assert html_response(conn, 200) =~ "Edit Book"
    end
  end

  describe "update book" do
    setup [:create_book]

    test "redirects to login when no admin signed in", %{conn: conn, book: book} do
      conn = put(conn, Routes.book_path(conn, :update, book), book: update_attrs(book.author, book.category))
      assert redirected_to(conn) == expected_redirect_conn(conn, :no_request_path)
    end    

    test "redirects when data is valid", %{protected_conn: conn, book: book} do
      conn = put(conn, Routes.book_path(conn, :update, book), book: update_attrs(book.author, book.category))
      assert redirected_to(conn) == Routes.admin_path(conn, :show)
    end

    test "renders errors when data is invalid", %{protected_conn: conn, book: book} do
      conn = put(conn, Routes.book_path(conn, :update, book), book: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Book"
    end
  end

  describe "delete book" do
    setup [:create_book]

    test "redirects to login when no admin signed in", %{conn: conn, book: book} do
      conn = delete(conn, Routes.book_path(conn, :delete, book))
      assert redirected_to(conn) == expected_redirect_conn(conn, :no_request_path)
    end

    test "deletes chosen book", %{protected_conn: conn, book: book} do
      conn = delete(conn, Routes.book_path(conn, :delete, book))
      assert redirected_to(conn) == Routes.admin_path(conn, :show)
    end
  end

  defp create_book(_) do
    book = fixture(:book)
    %{book: book |> Repo.preload([:author, :category])}
  end
end
