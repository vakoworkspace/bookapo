defmodule BookapoAdminWeb.AdminControllerTest do
  use BookapoAdminWeb.ConnCase

  setup %{conn: conn} do
    admin = %Bookapo.Accounts.Admin{username: "testuser"}
    conn = Pow.Plug.assign_current_user(conn, admin, otp_app: :bookapo_admin)

    {:ok, protected_conn: conn}
  end

  describe "GET /admin" do
    test "when admin signed in should show dashboard", %{protected_conn: conn} do
      conn = get(conn, "/admin")
      assert html_response(conn, 200) =~ "dashboard"
    end

    test "when no admin signed in, should redirect to sign in", %{conn: conn} do
      conn = get(conn, "/admin")
      assert redirected_to(conn) == Routes.pow_session_path(conn, :new, request_path: Phoenix.Controller.current_path(conn))
    end
  end
end
