defmodule BookapoAdminWeb.CategoryControllerTest do
  use BookapoAdminWeb.ConnCase

  alias Bookapo.Library
  alias Bookapo.Accounts.Admin

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  setup %{conn: conn} do
    admin = %Admin{username: "testexample"}
    conn = Pow.Plug.assign_current_user(conn, admin, otp_app: :bookapo)

    {:ok, protected_conn: conn}
  end

  defp expected_redirect_conn(conn, :with_request_path), do: Routes.pow_session_path(conn, :new, request_path: Phoenix.Controller.current_path(conn))
  defp expected_redirect_conn(conn, :no_request_path), do: Routes.pow_session_path(conn, :new)

  def fixture(:category) do
    {:ok, category} = Library.create_category(@create_attrs)
    category
  end

  describe "new category" do
    test "redirects when no admin signed in", %{conn: conn} do
      conn = get(conn, Routes.category_path(conn, :new))
      assert redirected_to(conn) == expected_redirect_conn(conn, :with_request_path)
    end

    test "renders form", %{protected_conn: conn} do
      conn = get(conn, Routes.category_path(conn, :new))
      assert html_response(conn, 200) =~ "New Category"
    end
  end

  describe "create category" do
    test "redirects to login when no admin signed in", %{conn: conn} do
      conn = post(conn, Routes.category_path(conn, :create), category: @create_attrs)
      assert redirected_to(conn) == expected_redirect_conn(conn, :no_request_path)
    end

    test "redirects to index of books when data is valid", %{protected_conn: conn} do
      conn = post(conn, Routes.category_path(conn, :create), category: @create_attrs)
      assert redirected_to(conn) == Routes.admin_path(conn, :show)
    end

    test "renders errors when data is invalid", %{protected_conn: conn} do
      conn = post(conn, Routes.category_path(conn, :create), category: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Category"
    end
  end

  describe "edit category" do
    setup [:create_category]

    test "redirects to login when no admin signed in", %{conn: conn, category: category} do
      conn = get(conn, Routes.category_path(conn, :edit, category))
      assert redirected_to(conn) == expected_redirect_conn(conn, :with_request_path)
    end

    test "renders form for editing chosen category", %{protected_conn: conn, category: category} do
      conn = get(conn, Routes.category_path(conn, :edit, category))
      assert html_response(conn, 200) =~ "Edit Category"
    end
  end

  describe "update category" do
    setup [:create_category]

    test "redirects to login when no admin signed in", %{conn: conn, category: category} do
      conn = put(conn, Routes.category_path(conn, :update, category), category: @update_attrs)
      assert redirected_to(conn) == expected_redirect_conn(conn, :no_request_path)
    end

    test "redirects when data is valid", %{protected_conn: conn, category: category} do
      conn = put(conn, Routes.category_path(conn, :update, category), category: @update_attrs)
      assert redirected_to(conn) == Routes.admin_path(conn, :show)
    end

    test "renders errors when data is invalid", %{protected_conn: conn, category: category} do
      conn = put(conn, Routes.category_path(conn, :update, category), category: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Category"
    end
  end

  describe "delete category" do
    setup [:create_category]

    test "redirects to login when no admin signed in", %{conn: conn, category: category} do
      conn = delete(conn, Routes.category_path(conn, :delete, category))
      assert redirected_to(conn) == expected_redirect_conn(conn, :no_request_path)
    end

    test "deletes chosen category", %{protected_conn: conn, category: category} do
      conn = delete(conn, Routes.category_path(conn, :delete, category))
      assert redirected_to(conn) == Routes.admin_path(conn, :show)
    end
  end

  defp create_category(_) do
    category = fixture(:category)
    %{category: category}
  end
end
